# ENVIRONMINT® - Continuous Delivery Automation for Oracle
#
# Copyright © 2014 LimePoint. All rights reserved.
#
# This file and its contents are confidential and owned by LimePoint.
# Only licenced users are permitted to access and use of this file.
# This file (or any part of it) may not be disclosed, copied or used
# except as expressly permitted in LimePoint’s End User Licence Agreement.
#
# LimePoint® and ENVIRONMINT® are Registered Trademarks of LimePoint Pty Ltd.
# For more information contact LimePoint at http://www.limepoint.com


require 'ffi_yajl'
require 'json'
require 'rest-client'


#pretty print JSON.pretty_generate(results)

module OracleCloudStackMgrClient
  class StackMgrClient
    attr_reader :identity_domain, :password, :username ,:api_url, :retries

    def initialize(opts)
      @api_url         = opts[:api_url]
       ##api_url = https://jaas.oraclecloud.com/paas/service/soa/api/v1.1
      @identity_domain = opts[:identity_domain]
      @username        = opts[:username]
      @password        = opts[:password]
      @retries        = opts[:retries]

      validate_client_options!
    end

    #################################
    #
    # methods to other API objects
    #
  
    def provisionservices(*args)
     OracleCloudStackMgrClient::OCSProvisionServices.new(self,*args)
    end

    #################################
    #
    # client methods
    #
    #################################
    
    def validate_client_options!
      raise ArgumentError, 'Username, password and identity_domain are required' if
        @username.nil? || @password.nil? || @identity_domain.nil?
        
      raise ArgumentError, 'An API URL is required' if @api_url.nil?
      raise ArgumentError, "API URL #{@api_url} is not a valid URI." unless valid_uri?(@api_url)
    end

    def valid_uri?(uri)
      uri = URI.parse(uri)
      uri.is_a?(URI::HTTP)
    rescue URI::InvalidURIError
      false
    end
    
  end
end
