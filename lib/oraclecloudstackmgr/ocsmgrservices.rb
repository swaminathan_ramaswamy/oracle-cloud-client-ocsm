# ENVIRONMINT® - Continuous Delivery Automation for Oracle
#
# Copyright © 2014 LimePoint. All rights reserved.
#
# This file and its contents are confidential and owned by LimePoint.
# Only licenced users are permitted to access and use of this file.
# This file (or any part of it) may not be disclosed, copied or used
# except as expressly permitted in LimePoint’s End User Licence Agreement.
#
# LimePoint® and ENVIRONMINT® are Registered Trademarks of LimePoint Pty Ltd.
# For more information contact LimePoint at http://www.limepoint.com

module OracleCloudStackMgrClient
  class OCSMgrServices

    attr_reader  :results,:response,:respCode,:responseMap,:dbaName, :dbaPassword, :forceDelete, :skipBackupOnTerminate,:client,:index,:servicedetailsmap, :opts,:instance_data,:service_id,:serviceName, :level, :topology, :subscriptionType, :description, :provisionOTD,:cloudStorageContainer, :cloudStorageUser, :cloudStoragePassword

    def initialize(client)
      @client = client
      @index = []
      @deleteresponsemap = nil
      @nil_payload = @nil_queryparam = nil
      @client.retries.nil? ? @retries=1 : @retries=@client.retries
      @retries=@retries.to_i
      @path = "/paas/api/v1.1"
    end

    def validate_view_options
      raise ArgumentError, 'service_id is required' if @service_id.nil?
    end

    def invoke_form(method,path,form_parameters,op_type,queryparams)
      @method=method
      @response_code=nil
      #RestClient.log='stdout'
      @index=[]
      @type=op_type
      responseMap = Hash.new
      invoke_counter=0

      begin
        invoke_counter=invoke_counter+1


          payload = Hash.new
          # payload.store("multipart", "true")
          # form_parameters.each do  |key,value|
            #  payload.store(key,value) if !value.nil?
          # end


        requestHeaders = request_headers(type: @type)
        requestHeaders.store("params",queryparams) if !queryparams.nil?

        puts "**** RestClient: method: " + @method.inspect
        puts "**** RestClient: url: " + full_url(path).inspect
        puts "**** RestClient: headers: " + requestHeaders.inspect
        puts "**** RestClient: user: " + @client.username.inspect
        puts "**** RestClient: form_parameters: " + form_parameters.to_s
        # puts "**** RestClient: payload: " + payload_hash.to_s

        if !form_parameters.nil?
        response = RestClient::Request.execute(method: @method ,
        url: full_url(path),
        headers: requestHeaders,
        user: @client.username,
        password: @client.password,
        payload: form_parameters
        )
        else
        # tweak to handle empty form_parameters
        requestHeaders.store('Content-Type', 'application/json')
        puts "#{requestHeaders}"
        response = RestClient::Request.execute(method: @method ,
                url: full_url(path),
                headers: requestHeaders,
                user: @client.username,
                password: @client.password )
        end

       # response = request.execute
       # puts "response headers = #{response.headers}"

      response_code = response.code

      rescue => e
          #hack for Request Timeout
          puts "e #{e}"
        if e.response.nil? && e.to_s.eql?("Request Timeout")
           results = e.to_s
           responseMap.store('error', results)
           responseMap.store('response', '408')  #hard-code to 408
        else

        response_code = e.response.code


          if e.response.include? "SM-UI-PROXY error: SM-SUE-302: null"
          puts "RETRY_ERROR #{e.response}" if invoke_counter <= @retry_counter
          retry if invoke_counter <=@retry_counter
        else
             begin
                !e.response.empty? ? results = FFI_Yajl::Parser.parse(e.response) : raise
              rescue => e1
               e.response.empty? ? results = e.response.code.to_s+' '+e.response.message :  results = e.response
               #special condition to handle exception that contain HTML content
                              #special condition to handle exception that contain HTML content
               if  results.include? "<!DOCTYPE"
                   results = e.response.code.to_s+' '+e.response.message
               end

             end


           #the exception results can be
           #JSON - { reason"=>"Resource Busy. Service usoracle66248/soa11g1 has active jobs }
           #message - Service with 'usoracle66248:soa11g'already exists
           #empty message with only code 404,409 etc ...

           #!e.response.empty?  - response is not empty - try to parse the response via
           #results = FFI_Yajl::Parser.parse(e.response) , if the response is a HTML message , parse will fail
           #rescue =>e1 , set the HTML response as is (results=e.response)
           #e.response.empty? - raise exception
           #rescue =>e1 , set the code + message as response content is empty

           #store error message to the map
           #results can either be JSON,HTML message or error code+message
           #puts "results = #{results}"
           responseMap.store('error', results)
           responseMap.store('response', response_code)      #include response as well
          end
        end
       else
              #response can be empty for API's which don't return any body
              #(e.g) createinstance , performop
              #in such case ,set results to response code (200/202)
              #in other cases parse the response
              #the response can be either Hash(predominantly) or Array(viewcomputenodes)
            begin
            response.empty? ? results = response.code.to_s :  results = FFI_Yajl::Parser.parse(response)

               # the response can also be something like : Accepted  (i.e) not empty , but also not parsable
               # we will rescue and return the response as is
                rescue => parse_error
                  results = response.code.to_s+' '+response.to_s
            end

      ensure
          #dummy ensure

    end

    if  results.is_a?(Hash)
      #either a JSON response or JSON exception
      #cases where exception is an hash - "reason"=>"Resource Busy. Service usoracle66248/soa11g1 has active jobs
      if !responseMap['error'].nil?
          #this means that the result was an exception hash , return it as-is , it'll contain error as
          #error=>"reason"=>"Resource Busy
          return responseMap
        # success check will not work this way
      else
      results.store('response', response_code)
      results.store("Location", response.headers[:location]) if !response.nil? && !response.headers[:location].nil?
      return results
      end

    else
       # should be an error case or array , error would have been handled in rescue block
       # add it to the map only if it' any array or response coden(202,200) etc

      if responseMap['error'].nil?
       #not error - array or response=>200/202 ?
          if !results.is_a?(String)
                #result is array
                responseMap.store('array_resp', results)
          end
        responseMap.store("response", response_code)

      else
      #error condition - 404/401 etc
      #nothing to add as response element has already been included
      end

      responseMap.store("Location", response.headers[:location]) if !response.nil? && !response.headers[:location].nil?
      responseMap
    end

    end

    def invoke(method,path,payload,op_type,queryparams)
      @method=method
      @response_code=nil
      #RestClient.log='stdout'
      @index=[]
      @type=op_type
      responseMap = Hash.new
      invoke_counter=0

      begin
        invoke_counter=invoke_counter+1
        if !payload.nil?
          @payload = payload.to_json
        else

          @payload = @nil_payload
        end

        requestHeaders = request_headers(type: @type)
        requestHeaders.store("params",queryparams) if !queryparams.nil?

        puts "**** RestClient: method: " + @method.inspect
        puts "**** RestClient: url: " + full_url(path).inspect
        puts "**** RestClient: headers: " + requestHeaders.inspect
        puts "**** RestClient: user: " + @client.username.inspect
        puts "**** RestClient: payload: " + @payload.inspect

        response = RestClient::Request.execute(method: @method ,
        url: full_url(path),
        headers: requestHeaders,
        user: @client.username,
        password: @client.password,
        payload: @payload)

       #puts "response headers = #{response.headers}"

      response_code = response.code

      rescue => e
          #hack for Request Timeout
        if e.response.nil? && e.to_s.eql?("Request Timeout")
           results = e.to_s
           responseMap.store('error', results)
           responseMap.store('response', '408')  #hard-code to 408 
        else 

        response_code = e.response.code 

          if e.response.include? "SM-UI-PROXY error: SM-SUE-302: null"
          puts "RETRY_ERROR #{e.response}" if invoke_counter <= @retries
          retry if invoke_counter <=@retries
        else
             begin
                !e.response.empty? ? results = FFI_Yajl::Parser.parse(e.response) : raise 
              rescue => e1
               e.response.empty? ? results = e.response.code.to_s+' '+e.response.message :  results = e.response
               #special condition to handle exception that contain HTML content
                              #special condition to handle exception that contain HTML content
               if  results.include? "<!DOCTYPE"
                   results = e.response.code.to_s+' '+e.response.message 
               end

             end
             
 
           #the exception results can be 
           #JSON - { reason"=>"Resource Busy. Service usoracle66248/soa11g1 has active jobs }
           #message - Service with 'usoracle66248:soa11g'already exists
           #empty message with only code 404,409 etc ...
           
           #!e.response.empty?  - response is not empty - try to parse the response via
           #results = FFI_Yajl::Parser.parse(e.response) , if the response is a HTML message , parse will fail
           #rescue =>e1 , set the HTML response as is (results=e.response)
           #e.response.empty? - raise exception
           #rescue =>e1 , set the code + message as response content is empty
           
           #store error message to the map
           #results can either be JSON,HTML message or error code+message
           #puts "results = #{results}"
           responseMap.store('error', results)
           responseMap.store('response', response_code)      #include response as well
          end
        end
       else
              #response can be empty for API's which don't return any body
              #(e.g) createinstance , performop
              #in such case ,set results to response code (200/202)
              #in other cases parse the response
              #the response can be either Hash(predominantly) or Array(viewcomputenodes)
            begin
            response.empty? ? results = response.code.to_s :  results = FFI_Yajl::Parser.parse(response)

               # the response can also be something like : Accepted  (i.e) not empty , but also not parsable 
               # we will rescue and return the response as is
                rescue => parse_error
                  results = response.code.to_s+' '+response.to_s
            end

      ensure
          #dummy ensure

end
     
    if  results.is_a?(Hash)
      #either a JSON response or JSON exception 
      #cases where exception is an hash - "reason"=>"Resource Busy. Service usoracle66248/soa11g1 has active jobs
      if !responseMap['error'].nil?  
          #this means that the result was an exception hash , return it as-is , it'll contain error as
          #error=>"reason"=>"Resource Busy    
          return responseMap
        # success check will not work this way 
      else
      results.store('response', response_code)      
      results.store("Location", response.headers[:location]) if !response.nil? && !response.headers[:location].nil?
      return results
      end
      
    else
       # should be an error case or array , error would have been handled in rescue block
       # add it to the map only if it' any array or response coden(202,200) etc

      if responseMap['error'].nil?
       #not error - array or response=>200/202 ?
          if !results.is_a?(String)
                #result is array
                responseMap.store('array_resp', results)
          end
        responseMap.store("response", response_code)

      else
      #error condition - 404/401 etc
      #nothing to add as response element has already been included
      end

      responseMap.store("Location", response.headers[:location]) if !response.nil? && !response.headers[:location].nil?
      responseMap
    end

    end

    def success(responseMap)
      responseMap['response'] == 200 || responseMap['response'] == 202 ? true:false
    end

    def trim_payload(payload)
      puts "payload = #{@payload}"
      payload.each do  |key,value|
        puts "value #{value}"
       payload.delete(key) if value.nil?
      end
    end

    def empty_query_params
      {

      }
    end

   def empty_payload
     @emptyPayload = {
      }
   end

    def populate_component_values
     @components = {

      }
    end

    def full_url(path)
      @client.api_url + path
    end

    def request_headers(opts = {})
       if opts[:type] == 'create'
         headers = { 'X-ID-TENANT-NAME' => @client.identity_domain ,'Content-Type' => 'application/vnd.com.oracle.oracloud.provisioning.Service+json','Accept' => 'application/vnd.com.oracle.oracloud.provisioning.Service+json'}
       elsif opts[:type] == 'form'
        headers = { 'X-ID-TENANT-NAME' => @client.identity_domain, 'Content-Type' => 'multipart/form-data'}
       else
         headers = { 'X-ID-TENANT-NAME' => @client.identity_domain ,'Content-Type' => 'application/json' ,'Accept' => 'application/json' }
       end
       headers
    end

  end
end
