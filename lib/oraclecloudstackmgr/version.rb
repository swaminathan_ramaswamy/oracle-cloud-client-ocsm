# ENVIRONMINT® - Continuous Delivery Automation for Oracle
#
# Copyright © 2014 LimePoint. All rights reserved.
#
# This file and its contents are confidential and owned by LimePoint.
# Only licenced users are permitted to access and use of this file.
# This file (or any part of it) may not be disclosed, copied or used
# except as expressly permitted in LimePoint’s End User Licence Agreement.
#
# LimePoint® and ENVIRONMINT® are Registered Trademarks of LimePoint Pty Ltd.
# For more information contact LimePoint at http://www.limepoint.com


module OracleCloudStackMgr
  VERSION = '1.0.0'.freeze
end
