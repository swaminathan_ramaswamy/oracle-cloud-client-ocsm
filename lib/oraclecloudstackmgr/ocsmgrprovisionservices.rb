# ENVIRONMINT® - Continuous Delivery Automation for Oracle
#
# Copyright © 2014 LimePoint. All rights reserved.
#
# This file and its contents are confidential and owned by LimePoint.
# Only licenced users are permitted to access and use of this file.
# This file (or any part of it) may not be disclosed, copied or used
# except as expressly permitted in LimePoint’s End User Licence Agreement.
#
# LimePoint® and ENVIRONMINT® are Registered Trademarks of LimePoint Pty Ltd.
# For more information contact LimePoint at http://www.limepoint.com

require 'oraclecloudstackmgr/ocsmgrservices'

module OracleCloudStackMgrClient
  class OCSProvisionServices <  OCSMgrServices

    attr_reader  :verbose,:isVerbose,:client,:results,:nil_payload,:method,:response,:payload,:responseMap,:serviceId,:jobId,:index,:params

    # Create a Stack
    def create(opts)
      description =opts[:description]
      name =opts[:name]
      parameterFile =opts[:parameterFile]
      parameterValues =opts[:parameterValues]
      tags = opts[:tags]
      template =opts[:template]

      path = @path + "/instancemgmt/#{client.identity_domain}/services/stack/instances"

      # parameterValues_hash = {
        #                :sshPublicKey => "key for oraclecs",
         #               :serviceName => "test",
          #              :dbShape => "oc3"
        #           }

       form_parameters = {
          :multipart => true,
          :name => name,
          :template => template,
          :description => description,
          :parameterFile => parameterFile
        }

        # handle optional attributes that needs to of in json format
        # :tags => tags.to_json,
        # :parameterValues => parameterValues.to_json,
        form_parameters['parameterValues'] = parameterValues.to_json if !parameterValues.nil?

        form_parameters.each do  |key,value|
        puts "value #{value}"
             form_parameters.delete(key) if value.nil?
        end

      # form_parameters  = create_payload

      puts "form_parameters #{form_parameters}"
      responseMap = invoke_form('post',path,form_parameters,'form', @nil_queryparam)

      # if success(responseMap)
        # OracleCloudSOACS::Status.new(responseMap)
        # else
        # OracleCloudSOACS::Status.new(responseMap)
       # end

    end

    # Start a Stack
    def start(opts)
      stackName =opts[:stackName]
      configPayload =opts[:configPayload]
      raise ArgumentError, 'stackName is required' if stackName.nil?

      path = @path + "/instancemgmt/#{client.identity_domain}/services/stack/instances/#{stackName}/start"


        form_parameters = nil
         if !configPayload.nil?
          form_parameters = {
                  :multipart=>true,
                  :'config-payload'=>configPayload
            }
         end


        # form_parameters.each do  |key,value|
        # puts "value #{value}"
          #   form_parameters.delete(key) if value.nil?
        # end

      puts "form_parameters #{form_parameters}"
      responseMap = invoke_form('post',path,form_parameters,'form', @nil_queryparam)

      # if success(responseMap)
        # OracleCloudSOACS::Status.new(responseMap)
        # else
        # OracleCloudSOACS::Status.new(responseMap)
       # end

    end

  # Delete a Stack
    def delete(opts)
      stackName =opts[:stackName]
      configPayload =opts[:configPayload]
      force =opts[:force]
      retainResources =opts[:retainResources]

      raise ArgumentError, 'stackName is required' if stackName.nil?


      path = @path + "/instancemgmt/#{client.identity_domain}/services/stack/instances/#{stackName}"

     form_parameters = nil
        if !configPayload.nil?
         form_parameters = {
                 :multipart=>true,
                 :'config-payload'=>configPayload
           }
        end

        # populate defaults
        force = true if force.nil? # By default, Cloud Stack forcibly deletes all resources in the stack if an error occurs

        queryParams = {}
            queryParams.store('force',force)
            queryParams.store('retain-resources',retainResources) if !retainResources.nil?

            puts "queryParams #{queryParams}"

      puts "form_parameters #{form_parameters}"
      responseMap = invoke_form('put',path,form_parameters,'form')

      # if success(responseMap)
        # OracleCloudSOACS::Status.new(responseMap)
        # else
        # OracleCloudSOACS::Status.new(responseMap)
       # end

    end
  end
end
