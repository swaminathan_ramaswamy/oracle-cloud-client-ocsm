# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'oraclecloudstackmgr/version'

Gem::Specification.new do |spec|
  spec.name          = 'oraclecloudstackmgr'
  spec.version       = '1.0.0'
  spec.authors       = ['LimePoint Pty Ltd']
  spec.email         = ['support@limepoint.com']

  spec.summary       = 'Client gem for interacting with the Oracle Stack ManagerCloud API.'
  spec.homepage      = 'https://bitbucket.org/cohesion/oracle-cloud-client-ocsm'

  #spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.files         =  ["Gemfile",
  "oraclecloudstackmgr.gemspec",
  "Rakefile",
  "lib/oraclecloudstackmgr.rb",
  "lib/oraclecloudstackmgr/version.rb",
  "lib/oraclecloudstackmgr/ocsmgrclient.rb",
  "lib/oraclecloudstackmgr/ocsmgrservices.rb",
  "lib/oraclecloudstackmgr/ocsmgrprovisionservices.rb"]

  spec.add_dependency 'rest-client', '~> 1.8'
  spec.add_dependency 'ffi-yajl',    '~> 2.2'

  spec.add_development_dependency 'rake',    '~> 10.0'
  spec.add_development_dependency 'rspec',   '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.35'
  spec.add_development_dependency 'pry'
end


